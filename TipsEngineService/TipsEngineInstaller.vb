Imports System.ComponentModel
Imports System.Configuration.Install

<System.ComponentModel.RunInstallerAttribute(True)> Public Class TipsEngineInstaller
    Inherits System.Configuration.Install.Installer

#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Installer overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents TipsProcessInstaller As System.ServiceProcess.ServiceProcessInstaller
    Friend WithEvents TipsServiceInstaller As System.ServiceProcess.ServiceInstaller
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.TipsProcessInstaller = New System.ServiceProcess.ServiceProcessInstaller
        Me.TipsServiceInstaller = New System.ServiceProcess.ServiceInstaller
        '
        'TipsProcessInstaller
        '
        Me.TipsProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.TipsProcessInstaller.Password = Nothing
        Me.TipsProcessInstaller.Username = Nothing
        '
        'TipsServiceInstaller
        '
        Me.TipsServiceInstaller.ServiceName = "NTB_TipsEngine"
        '
        'TipsEngineInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.TipsProcessInstaller, Me.TipsServiceInstaller})

    End Sub

#End Region

End Class
