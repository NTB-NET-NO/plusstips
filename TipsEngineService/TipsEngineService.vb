Imports System.ServiceProcess
Imports System.IO
Imports System.Net
Imports System.Web.Mail
Imports System.Text
Imports System.Xml
Imports System.Configuration.ConfigurationSettings

Public Class TipsEngineService
    Inherits System.ServiceProcess.ServiceBase

#Region " Service Globals "
    'Day constants
    Dim days As String() = {"S�ndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "L�rdag"}

    'Folders
    Dim langoddsFolder As String
    Dim tippebladDOCFolder As String
    Dim tippebladXMLFolder As String
    Dim tippingXMLFolder As String
    Dim logFolder As String
    Dim createdDOCFolder As String

    'Logs
    Dim systemLog As StreamWriter
    Dim errorLog As StreamWriter

    'Options
    Dim doLangodds As Boolean = False
    Dim downloadXML As Boolean = False
    Dim convert2XML As Boolean = False
    Dim createDocs As Boolean = False
    Dim sendDocs As Boolean = False

    Dim interval As Integer

    'Debug
    Dim sendError As Boolean
    Dim errorEmail As String

    'Document creation stuff
    Dim convertTipsDoc2XML As TipsDoc2XML
    Dim docCreator As CreateTipsDocuments
    Dim docLangodds As CreateLangoddsDocuments

    'Other module-wide stuff
    Dim xmlToFetch As ArrayList
    Dim docsToSend As ArrayList

    'Service action control variables
    Dim downloadSchedule As Integer
    Dim lastXMLDownload As Date

#End Region

#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

    'UserService overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ' The main entry point for the process
    <MTAThread()> _
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New TipsEngineService}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
    Friend WithEvents PollTimer As System.Timers.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.PollTimer = New System.Timers.Timer
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PollTimer
        '
        Me.PollTimer.Interval = 300000
        '
        'TipsEngineService
        '
        Me.ServiceName = "TipsEngine"
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    'Starts the service
    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Initiate
        Initiate()

        'Launch timers
        PollTimer.Interval = 5000
        PollTimer.Enabled = True

    End Sub

    'Stops the service
    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        PollTimer.Enabled = False

        'Done, cleanup
        convertTipsDoc2XML = Nothing
        docCreator = Nothing
        systemLog.WriteLine(Now & " : -- Service ended --")
        systemLog.Close()

    End Sub

    'Shutdown
    Protected Overrides Sub OnShutdown()
        OnStop()
    End Sub

    'Handles timed actions
    Private Sub PollTimer_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles PollTimer.Elapsed

        'Stop timer, log timer event
        PollTimer.Enabled = False
        'systemLog.WriteLine(Now & " : POLLTIMER - Timer kicked...")

        'Download xml and create vurderings-docs
        If downloadXML And _
           Now.Hour = downloadSchedule And Today.Subtract(lastXMLDownload).TotalDays > 0 Then

            DownloadItems()
            lastXMLDownload = Now()
        End If

        'Langodds
        If doLangodds Then CreateLangodds()

        'Faktadocs
        If convert2XML Then ConvertTippebladDocs()
        If createDocs Then CreateFaktaDocs()

        'Email distribution
        If sendDocs Then SendDocsByMail()

        'Start the timer again
        PollTimer.Interval = interval
        PollTimer.Enabled = True
    End Sub

#Region " General Service functions "

    'Initiate service, load settings
    Sub Initiate()

        'Load settings
        langoddsFolder = AppSettings("langoddsFolder")
        tippebladDOCFolder = AppSettings("tippebladDOCFolder")
        tippebladXMLFolder = AppSettings("tippebladXMLFolder")
        tippingXMLFolder = AppSettings("tippingXMLFolder")
        createdDOCFolder = AppSettings("createdDOCFolder")
        logFolder = AppSettings("logFolder")

        interval = AppSettings("interval") * 1000

        If AppSettings("createLangodds") = "true" Then doLangodds = True
        If AppSettings("downloadXML") = "true" Then downloadXML = True
        If AppSettings("convert2XML") = "true" Then convert2XML = True
        If AppSettings("createDocs") = "true" Then createDocs = True
        If AppSettings("sendDocs") = "true" Then sendDocs = True

        'XML converter
        convertTipsDoc2XML = New TipsDoc2XML
        convertTipsDoc2XML.outputFolder = tippebladXMLFolder

        'Doc creator
        docCreator = New CreateTipsDocuments
        docCreator.outputFolder = createdDOCFolder

        'Langodds
        docLangodds = New CreateLangoddsDocuments
        docLangodds.outputFolder = createdDOCFolder

        'Modulewide arraylists
        xmlToFetch = New ArrayList
        docsToSend = New ArrayList

        'Debug
        sendError = AppSettings("emailOnError")
        errorEmail = AppSettings("errorEmail")

        'Load XML-addresses to fetch
        Dim n
        For n = 1 To AppSettings("count")
            xmlToFetch.Add(AppSettings("item" & n))
        Next

        downloadSchedule = AppSettings("downloadSchedule")
        lastXMLDownload = Today.AddDays(-1)

        'Set up logging
        systemLog = New StreamWriter(logFolder & "download-log.txt", True, Encoding.GetEncoding("iso-8859-1"))
        systemLog.AutoFlush = True
        systemLog.WriteLine(Now & " : -- Service started --")

    End Sub


    Sub SendErrorEmail(ByVal msg As String, ByVal ex As Exception)

        Dim epost As MailMessage = New MailMessage
        SmtpMail.SmtpServer = AppSettings("smtp")

        If sendError Then
            Try
                epost.From = AppSettings("fromEmail")
                epost.To = errorEmail
                epost.Subject = "FEIL i Tipsengine!"
                epost.BodyFormat = Web.Mail.MailFormat.Text
                epost.Body = "Det har oppst�tt en feil i forbindelse med produksjon av tippetips-dokumenter." & vbCrLf & vbCrLf
                epost.Body &= msg & vbCrLf & vbCrLf

                epost.Body &= Now & vbCrLf
                epost.Body &= ex.Message & vbCrLf
                epost.Body &= ex.StackTrace & vbCrLf

                SmtpMail.Send(epost)
            Catch
            End Try
        End If

    End Sub

    'Sends all created documents by email to the specified recipient
    Sub SendDocsByMail()

        'Send docs?
        If docsToSend.Count > 0 Then

            Dim epost As MailMessage = New MailMessage

            SmtpMail.SmtpServer = AppSettings("smtp")

            epost.From = AppSettings("fromEmail")
            epost.To = AppSettings("toEmail")
            epost.Subject = "Tippetips: Dokumenter fra NTB Pluss"
            epost.BodyFormat = Web.Mail.MailFormat.Text
            epost.Body = "F�lgende tipsdokumenter er vedlagt:" & vbCrLf & vbCrLf

            Dim s As String
            For Each s In docsToSend
                epost.Body += s & vbCrLf
                Dim att As MailAttachment = New MailAttachment(s)
                epost.Attachments.Add(att)
            Next

            Dim ok As Boolean = True
            Try
                SmtpMail.Send(epost)
            Catch e As Exception
                ok = False

                'Log the error
                systemLog.WriteLine(Now & " : EMAIL - Failed to send docs by email")

                errorLog = New StreamWriter(logFolder & "errorlog.txt", True, Encoding.GetEncoding("iso-8859-1"))
                errorLog.WriteLine(Now & " : " & e.Message & vbCrLf & e.StackTrace & vbCrLf & vbCrLf)
                errorLog.Close()
            End Try

            If ok Then
                systemLog.WriteLine(Now & " : EMAIL - Email with documents sent to '" & AppSettings("toEmail") & "'")
                docsToSend.Clear()
            End If

        End If

    End Sub

#End Region

#Region " Tippekupongen "

    'Downloads and saves items from Norsk Tipping. Vurderingsdokumenter created if specified.
    Sub DownloadItems()

        'HTTP and stream stuff
        Dim request As HttpWebRequest
        Dim response As HttpWebResponse
        Dim resReader As StreamReader
        Dim fileWriter As StreamWriter

        'XML content
        Dim xmlDoc As XmlDocument = New XmlDocument
        Dim content As String

        'Other helper vars
        Dim item As String
        Dim filename As String
        Dim dt As Date
        Dim week, day As String

        'Loop through the download object from the settings file and fetch them
        For Each item In xmlToFetch
            systemLog.WriteLine(Now & " : DOWNLOAD - Processing '" & item & "'")

            'Set up request
            request = WebRequest.Create(item)

            Try
                response = request.GetResponse()
            Catch e As WebException
                systemLog.WriteLine(Now & " : DOWNLOAD ERROR - " & e.Message)
                SendErrorEmail("Nedlasting av XML fra Norsk Tipping feilet.", e)
                GoTo Next_Item
            End Try

            resReader = New StreamReader(response.GetResponseStream(), Encoding.GetEncoding("iso-8859-1"))
            content = resReader.ReadToEnd()
            resReader.Close()

            'Check XML content
            Try
                xmlDoc.LoadXml(content)
            Catch e As XmlException
                systemLog.WriteLine(Now & " : DOWNLOAD ERROR - " & e.Message)
                'SendErrorEmail("XML fra Norsk Tipping er skadet eller i galt format.", e)
                content = ""
            End Try

            'Check/fetch nodes
            Try
                dt = xmlDoc.SelectSingleNode("/TIPPING/DRAW_DATE").InnerText
                week = xmlDoc.SelectSingleNode("/TIPPING/WEEKNBR").InnerText
            Catch e As Exception
                systemLog.WriteLine(Now & " : DOWNLOAD ERROR - " & e.Message)
                'SendErrorEmail("XML fra Norsk Tipping er skadet eller i galt format.", e)
                content = ""
            End Try

            'Check if we have any valid xml-data
            If content <> "" Then
                day = days(dt.DayOfWeek)
                If week.Length < 2 Then week = "0" & week

                filename = "week-" & week & "_" & day & "_" & dt.ToString("dd-MM-yyyy") & ".xml"

                fileWriter = New StreamWriter(tippingXMLFolder & filename, False, Encoding.GetEncoding("iso-8859-1"))
                fileWriter.Write(content)
                fileWriter.Close()

                systemLog.WriteLine(Now & " : DOWNLOAD - " & filename & " saved.")

                'Create vurderingsdokument
                If createDocs Then
                    Dim ok As Boolean = True
                    Dim targetFile As String
                    Try
                        targetFile = docCreator.CreateTipsVurderingsDocument(tippingXMLFolder & filename)
                    Catch e As Exception
                        ok = False
                        docCreator.Cleanup()

                        'Log the error
                        SendErrorEmail("Generering av Vurderingsdokument feilet. ( " & filename & " )", e)
                        systemLog.WriteLine(Now & " : DOC CREATION - Failed to create 'Vurderingsdokument' from " & filename)

                        errorLog = New StreamWriter(logFolder & "errorlog.txt", True, Encoding.GetEncoding("iso-8859-1"))
                        errorLog.WriteLine(Now & " : " & e.Message & vbCrLf & e.StackTrace & vbCrLf & vbCrLf)
                        errorLog.Close()
                    End Try

                    If ok Then
                        Dim span As TimeSpan = Now.Subtract(File.GetCreationTime(targetFile))
                        If span.TotalSeconds <= 10 Then docsToSend.Add(targetFile)
                        systemLog.WriteLine(Now & " : DOC CREATION - 'Vurderingsdokument' " & targetFile.Substring(targetFile.LastIndexOf("\") + 1) & " created.")
                    End If

                End If

            End If
Next_Item:
        Next

    End Sub

    'Creates XML docs from the tippeblad-docs in this folder 
    Sub ConvertTippebladDocs()

        'Loop the folder, create xml from docs
        Dim f, outfile As String
        Dim files As String() = Directory.GetFiles(tippebladDOCFolder)

        For Each f In files
            outfile = tippebladXMLFolder & f.Substring(f.LastIndexOf("\") + 1) & ".xml"
            systemLog.WriteLine(Now & " : XML CONVERSION - " & f.Substring(f.LastIndexOf("\") + 1) & " -> " & f.Substring(f.LastIndexOf("\") + 1) & ".xml")

            Dim ok As Boolean = True
            Try
                convertTipsDoc2XML.ConvertDoc2XML(f, outfile)
            Catch e As Exception
                ok = False
                convertTipsDoc2XML.Cleanup()

                'Log the error
                SendErrorEmail("Konvertering fra DOC (fra Tippebladet) til XML feilet. ( " & Path.GetFileName(f) & " )", e)
                systemLog.WriteLine(Now & " : XML CONVERSION - Failed to create XML '" & f.Substring(f.LastIndexOf("\") + 1) & ".xml'")

                errorLog = New StreamWriter(logFolder & "errorlog.txt", True, Encoding.GetEncoding("iso-8859-1"))
                errorLog.WriteLine(Now & " : " & e.Message & vbCrLf & e.StackTrace & vbCrLf & vbCrLf)
                errorLog.Close()
            End Try

            If ok Then
                If File.Exists(tippebladDOCFolder & "used\" & f.Substring(f.LastIndexOf("\") + 1)) Then
                    File.Delete(tippebladDOCFolder & "used\" & f.Substring(f.LastIndexOf("\") + 1))
                End If
                File.Move(f, tippebladDOCFolder & "used\" & f.Substring(f.LastIndexOf("\") + 1))

                systemLog.WriteLine(Now & " : XML CONVERSION - OK! " & f.Substring(f.LastIndexOf("\") + 1) & " moved to " & tippebladDOCFolder & "used\")
            End If
        Next

    End Sub

    'Create fakta-docs from based on the tippeblad XML
    Sub CreateFaktaDocs()

        'Loop the folder, create xml from docs
        Dim f, outfile As String
        Dim files As String() = Directory.GetFiles(tippebladXMLFolder)

        For Each f In files

            systemLog.WriteLine(Now & " : DOC CREATION - Processing '" & f.Substring(f.LastIndexOf("\") + 1) & "'")

            'Find matching TippingXML
            Dim week As String = f.Substring(f.IndexOf(".") - 2, 2)
            If Not IsNumeric(week) Then
                week = "0" & f.Substring(f.IndexOf(".") - 1, 1)
            End If

            Dim day = f.Substring(f.LastIndexOf("\") + 1, f.IndexOf(".") - 2 - f.LastIndexOf("\") - 1)

            Dim d As Integer
            If f.IndexOf("lor") > -1 Or f.IndexOf("l�r") > -1 Then
                d = 6
            ElseIf f.IndexOf("son") > -1 Or f.IndexOf("s�n") > -1 Then
                d = 0
            ElseIf f.IndexOf("man") > -1 Then
                d = 1
            Else
                d = 3
            End If

            Dim tf As String
            Dim tFiles As String() = Directory.GetFiles(tippingXMLFolder, "*week-" & week & "*" & Today.Year & "*")

            'For Each tf In tFiles
            Dim i As Integer
            For i = tFiles.GetUpperBound(0) To 0 Step -1
                tf = tFiles(i)
                If d <> 3 And tf.IndexOf(days(d)) > -1 Then
                    Exit For
                    'ElseIf d = 6 Then
                    'If tf.IndexOf(days(d - 1)) > -1 Then Exit For
                ElseIf d = 0 Then
                    If tf.IndexOf(days(d + 1)) > -1 Then Exit For
                ElseIf d = 3 And tf.IndexOf(days(0)) = -1 And tf.IndexOf(days(1)) = -1 And _
                  tf.IndexOf(days(6)) = -1 Then
                    Exit For
                End If
            Next

            If tf = "" Then

                Try
                    Dim ex As New Exception("Could not find an XML file to match '" & Path.GetFileName(f) & "'. Missing XML from Norsk Tipping.")
                    Throw ex
                Catch ex As Exception
                    'Log error
                    systemLog.WriteLine(Now & " : DOC CREATION - NO Matching Norsk Tipping XML found!")
                    errorLog = New StreamWriter(logFolder & "errorlog.txt", True, Encoding.GetEncoding("iso-8859-1"))
                    errorLog.WriteLine(Now & " : " & ex.Message & vbCrLf & ex.StackTrace & vbCrLf & vbCrLf)
                    errorLog.Close()

                    SendErrorEmail("Generering av Faktadokument feilet. ( " & Path.GetFileName(f) & " - " & Path.GetFileName(tf) & " )", ex)
                End Try

            Else
                systemLog.WriteLine(Now & " : DOC CREATION - Matching Norsk Tipping XML found, '" & tf & "'")

                'Create faktadokument
                Dim filename
                Dim ok As Boolean = True
                Try
                    filename = docCreator.CreateTipsFaktaDocument(tf, f)
                Catch e As Exception
                    ok = False
                    docCreator.Cleanup()

                    SendErrorEmail("Generering av Faktadokument feilet. ( " & Path.GetFileName(f) & " - " & Path.GetFileName(tf) & " )", e)

                    'Log error
                    systemLog.WriteLine(Now & " : DOC CREATION - Failed to create 'Faktadokument' from " & f)
                    errorLog = New StreamWriter(logFolder & "errorlog.txt", True, Encoding.GetEncoding("iso-8859-1"))
                    errorLog.WriteLine(Now & " : " & e.Message & vbCrLf & e.StackTrace & vbCrLf & vbCrLf)
                    errorLog.Close()
                End Try

                'Log completion
                If ok Then
                    systemLog.WriteLine(Now & " : DOC CREATION - 'Faktadokument' created from " & f)

                    'Dim span As TimeSpan = Now.Subtract(File.GetCreationTime(filename))
                    'If span.TotalSeconds <= 10 Then docsToSend.Add(filename)
                    docsToSend.Add(filename)

                    'Move the file if everything is okay
                    If File.Exists(tippebladXMLFolder & "used\" & f.Substring(f.LastIndexOf("\") + 1)) Then
                        File.Delete(tippebladXMLFolder & "used\" & f.Substring(f.LastIndexOf("\") + 1))
                    End If
                    File.Move(f, tippebladXMLFolder & "used\" & f.Substring(f.LastIndexOf("\") + 1))

                    systemLog.WriteLine(Now & " : DOC CREATION - " & f.Substring(f.LastIndexOf("\") + 1) & " moved to " & tippebladXMLFolder & "used\")
                End If


            End If

        Next

    End Sub

#End Region

#Region " Langodds "

    'Creates langodds documents from Norsk Tipping input
    Sub CreateLangodds()

        Dim f, outfile As String
        Dim files As String() = Directory.GetFiles(langoddsFolder)

        For Each f In files
            systemLog.WriteLine(Now & " : LANGODDS - " & Path.GetFileName(f))

            Dim ok As Boolean = True
            Try
                outfile = docLangodds.GenerateLangodds(f)
            Catch e As Exception
                ok = False

                'Log the error
                SendErrorEmail("Generering av Langoddsdokumenter feilet. ( " & Path.GetFileName(f) & " )", e)
                systemLog.WriteLine(Now & " : LANGODDS - Failed to create langodds from '" & Path.GetFileName(f))

                errorLog = New StreamWriter(logFolder & "errorlog.txt", True, Encoding.GetEncoding("iso-8859-1"))
                errorLog.WriteLine(Now & " : " & e.Message & vbCrLf & e.StackTrace & vbCrLf & vbCrLf)
                errorLog.Close()
            End Try

            If ok Then
                If File.Exists(langoddsFolder & "used\" & Path.GetFileName(f)) Then
                    File.Delete(langoddsFolder & "used\" & Path.GetFileName(f))
                End If

                'Dim span As TimeSpan = Now.Subtract(File.GetCreationTime(outfile))
                'If span.TotalSeconds <= 10 Then docsToSend.Add(outfile)

                docsToSend.Add(outfile)

                File.Move(f, langoddsFolder & "used\" & Path.GetFileName(f))

                systemLog.WriteLine(Now & " : LANGODDS - OK! " & Path.GetFileName(f) & " moved to " & langoddsFolder & "used\")
            End If
        Next

    End Sub

#End Region

End Class
