Imports System.IO
Imports System.Text
Imports Word

'Converts the documents from Tippebladet tips to XML
Public Class TipsDoc2XML

    Private wdApp As ApplicationClass
    Private wdDoc As DocumentClass

    Private rd As StreamReader
    Private wr As StreamWriter

    Public outputFolder As String

    'Constructor
    Public Sub New()
        wdApp = New Application()
    End Sub

    'Destructor
    Protected Overrides Sub Finalize()
        wdApp.Quit()
        wdApp = Nothing
        MyBase.Finalize()
    End Sub

    'Cleans up misc stuff to prevent errors
    Public Sub Cleanup()
        'Close doc
        Try
            wdDoc.Close(Word.WdSaveOptions.wdDoNotSaveChanges)
        Catch e As Exception
        End Try

        'Kill streams
        rd.Close()
        wr.Close()

        'Remove tempfile
        File.Delete(outputFolder & "tmp\tmp-doc.txt")
    End Sub

    'Main converter funtion
    Public Function ConvertDoc2XML(ByVal inFilename As String, ByVal outFilename As String) As Boolean

        'Variables
        Dim line As String

        'Open the DOC-file and save as text
        wdDoc = wdApp.Documents.Open(inFilename)
        wdDoc.SaveAs(outputFolder & "tmp\tmp-doc.txt", Word.WdSaveFormat.wdFormatText)
        wdDoc.Close()

        'Create streams
        rd = New StreamReader(outputFolder & "tmp\tmp-doc.txt", Encoding.GetEncoding("iso-8859-1"))
        wr = New StreamWriter(outFilename, False, Encoding.GetEncoding("iso-8859-1"))

        'Header
        wr.WriteLine("<?xml version='1.0' encoding='ISO-8859-1' standalone='yes' ?>")
        'wr.WriteLine("<?xml-stylesheet href='tips-format.xsl' type='text/xsl' ?>")
        wr.WriteLine("<nitf>")

        'Read - skipping blanks
        Do
            line = rd.ReadLine().Trim()
        Loop Until line <> ""

        'Head/title
        wr.WriteLine("<head><title>" & line & "</title></head>")
        wr.WriteLine("<body>")
        wr.WriteLine("<body.head><hedline><hl1>" & line & "</hl1></hedline></body.head>")

        wr.WriteLine("<body.content>")

        Do

            If line.ToUpper().StartsWith("FEM") Then
                'Five last matches history
                line = FemSisteKamper(line, rd, wr)
            ElseIf line.ToUpper().StartsWith("TABELLER") Then
                'Tables
                line = Tabeller(line, rd, wr)
            ElseIf line.ToUpper().StartsWith("FORM") Then
                'FormTables
                line = FormTabeller(line, rd, wr)
            ElseIf line.ToUpper().StartsWith("TILSVARENDE") Then
                'Simmilar matches
                line = TilsvarendeKamper(line, rd, wr)
            ElseIf line.ToUpper().StartsWith("TOPPSCORERE") Then
                'Toppscorere
                line = Toppscorere(line, rd, wr)
            ElseIf line.ToUpper().StartsWith("NESTE") Then
                'Next week's coupon
                line = HovedKupong(line, rd, wr)
            Else
                'This week
                line = HovedKupong(line, rd, wr)
            End If

        Loop While line <> ""


        'Footer
        wr.WriteLine("</body.content>")
        wr.WriteLine("</body>")
        wr.WriteLine("</nitf>")

        wr.Close()
        rd.Close()

        'Remove tempfile
        File.Delete(outputFolder & "tmp\tmp-doc.txt")

        Return True
    End Function


    'Checks if a line contains a defined section marker
    Protected Function TestSectionMarker(ByVal line As String) As Boolean

        Return line.ToUpper().StartsWith("FEM") Or _
               line.ToUpper().StartsWith("TABELLER") Or _
               line.ToUpper().StartsWith("FORM") Or _
               line.ToUpper().StartsWith("TOPP") Or _
               line.ToUpper().StartsWith("TILSVARENDE") Or _
               line.ToUpper().StartsWith("NESTE")

    End Function

    'Reads and parses the main tips coupon
    Protected Function HovedKupong(ByVal line As String, ByRef rd As StreamReader, ByRef wrt As StreamWriter) As String

        Dim i As Integer
        Dim arr As String()
        Dim cls As String

        If line.ToUpper().StartsWith("NESTE") Then
            cls = "neste_hovedkupong"
        Else
            cls = "hovedkupong"
        End If

        wrt.WriteLine("<hl2 class='" & cls & "'>" & line & "</hl2>")

        Do
            line = rd.ReadLine().Trim()
        Loop Until line <> ""

        'Kupongen - 12 kamper
        While (Not TestSectionMarker(line) Or cls = "neste_hovedkupong") And line <> ""

            wrt.WriteLine("<table class='" & cls & "'>")

            While line <> ""

                arr = line.Split(vbTab)

                wrt.WriteLine("<tr>")
                For i = 0 To arr.GetUpperBound(0)
                    If arr.GetValue(i).IndexOf("-") > -1 Then
                        Dim tmp As String = arr.GetValue(i).Replace("&", "&amp;")
                        wrt.WriteLine("<td>" & tmp.Split("-")(0).Trim & "</td>")
                        wrt.WriteLine("<td>" & tmp.Split("-")(1).Trim & "</td>")
                    Else
                        wrt.WriteLine("<td>" & arr.GetValue(i) & "</td>")
                    End If
                Next
                wrt.WriteLine("</tr>")

                If rd.Peek() <> -1 Then
                    line = rd.ReadLine().Trim()
                Else
                    line = ""
                End If

            End While

            wrt.WriteLine("</table>")

            'Read - skipping blanks
            While line = "" And rd.Peek() <> -1
                line = rd.ReadLine().Trim()
            End While

        End While

        Return line

    End Function

    'Reads and parses the five last matches stats
    Protected Function FemSisteKamper(ByVal line As String, ByRef rd As StreamReader, ByRef wrt As StreamWriter) As String

        Dim i, rows As Integer
        Dim arr As String()

        'Five last matches
        wrt.WriteLine("<hl2 class='femsiste'>" & line & "</hl2>")

        'Read - skipping blanks
        Do
            line = rd.ReadLine().Trim()
        Loop Until line <> ""

        'Check for new section
        While Not TestSectionMarker(line)

            wrt.WriteLine("<table class='femsiste'>")
            rows = 0

            While line <> ""
                arr = line.Split(vbTab)

                wrt.WriteLine("<tr>")
                For i = 0 To arr.GetUpperBound(0)
                    wrt.WriteLine("<td>" & arr.GetValue(i).Replace("&", "&amp;") & "</td>")
                Next
                wrt.WriteLine("</tr>")
                rows += 1

                'Next line
                line = rd.ReadLine().Trim()
            End While

            wrt.WriteLine("</table>")

            'Read - skipping blanks
            Do
                line = rd.ReadLine().Trim()
            Loop Until line <> ""

        End While

        Return line
    End Function

    'Reads and parses the league tables
    Protected Function Tabeller(ByVal line As String, ByRef rd As StreamReader, ByRef wrt As StreamWriter) As String

        Dim i As Integer
        Dim arr As String()

        'Tabeller
        wrt.WriteLine("<hl2 class='tabell'>" & line & "</hl2>")

        'Read - Skipping blanks
        Do
            line = rd.ReadLine().Trim()
        Loop Until line <> ""

        While Not TestSectionMarker(line)
            wrt.WriteLine("<hl3 class='tabell'>" & line & "</hl3>")
            wrt.WriteLine("<table class='tabell'>")

            'Table info
            line = rd.ReadLine().Trim()
            While line <> ""

                arr = line.Split(vbTab)

                wrt.WriteLine("<tr>")
                For i = 0 To arr.GetUpperBound(0)
                    If arr.GetValue(i).IndexOf("-") > -1 And i > 2 Then
                        Dim tmp As String = arr.GetValue(i).Replace("&", "&amp;")
                        wrt.WriteLine("<td>" & tmp.Split("-")(0).Trim & "</td>")
                        wrt.WriteLine("<td>-</td>")
                        wrt.WriteLine("<td>" & tmp.Split("-")(1).Trim & "</td>")
                    Else
                        wrt.WriteLine("<td>" & arr.GetValue(i).Replace("&", "&amp;").Trim() & "</td>")
                    End If
                Next
                wrt.WriteLine("</tr>")

                line = rd.ReadLine().Trim()
            End While

            wrt.WriteLine("</table>")

            Do
                line = rd.ReadLine().Trim()
            Loop Until line <> ""

        End While

        Return line
    End Function

    'Reads and parses the form/fitness tables
    Protected Function FormTabeller(ByVal line As String, ByRef rd As StreamReader, ByRef wrt As StreamWriter) As String

        Dim i As Integer
        Dim arr As String()

        'Tilsvarende(kamper)
        wrt.WriteLine("<hl2 class='formtabell'>" & line & "</hl2>")

        'Skip blanks
        Do
            line = rd.ReadLine().Trim()
        Loop Until line <> ""

        'Table data
        While Not TestSectionMarker(line)
            wrt.WriteLine("<table class='formtabell'>")

            While line <> ""
                arr = line.Split(vbTab)

                wrt.WriteLine("<tr>")
                For i = 0 To arr.GetUpperBound(0)
                    wrt.WriteLine("<td>" & arr.GetValue(i).Replace("&", "&amp;") & "</td>")
                Next
                wrt.WriteLine("</tr>")

                line = rd.ReadLine().Trim()
            End While

            wrt.WriteLine("</table>")

            'Read - skipping blanks
            Do
                line = rd.ReadLine().Trim()
            Loop Until line <> ""
        End While

        Return line

    End Function

    'Handles the tilsvarende kamper section
    Protected Function TilsvarendeKamper(ByVal line As String, ByRef rd As StreamReader, ByRef wrt As StreamWriter) As String

        Dim i As Integer
        Dim arr, arr2 As String()

        'Tilsvarende(kamper)
        wrt.WriteLine("<hl2 class='tilsvarende'>" & line & "</hl2>")

        'Skip blanks
        Do
            line = rd.ReadLine().Trim()
        Loop Until line <> ""

        While Not TestSectionMarker(line)

            'Table info
            wrt.WriteLine("<table class='tilsvarende'>")
            arr = line.Split(vbTab)
            arr2 = rd.ReadLine().Trim().Split(vbTab)

            'Header
            wrt.WriteLine("<tr><td></td><td></td>")
            For i = 0 To arr.GetUpperBound(0)
                If arr2.GetUpperBound(0) > 0 Then
                    wrt.WriteLine("<td>" & arr.GetValue(i).Trim() & arr2.GetValue(i).Trim() & "</td>")
                Else
                    wrt.WriteLine("<td>" & arr.GetValue(i).Trim() & "</td>")
                End If
            Next
            wrt.WriteLine("</tr>")

            'Skip blanks
            Do
                line = rd.ReadLine().Trim()
            Loop Until line <> ""

            'Match scores
            While line <> ""

                arr = line.Split(vbTab)

                wrt.WriteLine("<tr>")
                For i = 0 To arr.GetUpperBound(0)
                    wrt.WriteLine("<td>" & arr.GetValue(i).Replace("&", "&amp;") & "</td>")
                Next
                wrt.WriteLine("</tr>")

                line = rd.ReadLine().Trim()

            End While

            wrt.WriteLine("</table>")

            Do
                line = rd.ReadLine().Trim()
            Loop Until line <> ""

        End While

        Return line
    End Function

    'Reads and parses the top goalscorers data
    Protected Function Toppscorere(ByVal line As String, ByRef rd As StreamReader, ByRef wrt As StreamWriter) As String

        Dim i As Integer
        Dim arr As String()

        'Toppscorere
        wrt.WriteLine("<hl2 class='toppscorere'>" & line & "</hl2>")

        'Skip blanks
        Do
            line = rd.ReadLine().Trim()
        Loop Until line <> ""

        While Not TestSectionMarker(line) Or line.ToUpper().StartsWith("TOPP")

            'Table info
            wrt.WriteLine("<table class='toppscorere'>")

            While line <> ""
                arr = line.Split(vbTab)

                wrt.WriteLine("<tr>")
                For i = 0 To arr.GetUpperBound(0)
                    wrt.WriteLine("<td>" & arr.GetValue(i).Replace("&", "&amp;") & "</td>")
                Next
                wrt.WriteLine("</tr>")

                line = rd.ReadLine().Trim()
            End While

            wrt.WriteLine("</table>")

            Do
                line = rd.ReadLine().Trim()
            Loop While line = ""

        End While

        Return line
    End Function

End Class
