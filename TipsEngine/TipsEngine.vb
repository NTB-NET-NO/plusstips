Imports System.IO
Imports System.Net
Imports System.Web.Mail
Imports System.Text
Imports System.Xml
Imports System.Configuration.ConfigurationSettings

Module TipsEngine

    'Day constants
    Dim days As String() = {"S�ndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "L�rdag"}

    'Folders
    Dim tippebladDOCFolder As String
    Dim tippebladXMLFolder As String
    Dim tippingXMLFolder As String
    Dim logFolder As String
    Dim createdDOCFolder As String

    'Logs
    Dim systemLog As StreamWriter
    Dim errorLog As StreamWriter

    'Options
    Dim downloadXML As Boolean = False
    Dim convert2XML As Boolean = False
    Dim createDocs As Boolean = False
    Dim sendDocs As Boolean = False

    'Document creation stuff
    Dim convertTipsDoc2XML As TipsDoc2XML
    Dim docCreator As CreateTipsDocuments

    'Other module-wide stuff
    Dim xmlToFetch As ArrayList
    Dim docsToSend As ArrayList

    'Main program
    Sub Main()

        'Startup
        Initiate()

        'Set up logging
        systemLog = New StreamWriter(logFolder & "download-log.txt", True, Encoding.GetEncoding("iso-8859-1"))
        systemLog.AutoFlush = True
        systemLog.WriteLine(Now & " : -- Application started --")

        'Fetch XML
        If downloadXML Then DownloadItems()

        'Other tasks:
        If convert2XML Then ConvertTippebladDocs()
        If createDocs Then CreateFaktaDocs()

        'Send docs by email
        If sendDocs Then SendDocsByMail()

        'Done, cleanup
        convertTipsDoc2XML = Nothing
        docCreator = Nothing
        systemLog.WriteLine(Now & " : -- Application ended --")
        systemLog.Close()
    End Sub

    'Initiate program, load settings
    Sub Initiate()

        'Load settings
        tippebladDOCFolder = AppSettings("tippebladDOCFolder")
        tippebladXMLFolder = AppSettings("tippebladXMLFolder")
        tippingXMLFolder = AppSettings("tippingXMLFolder")
        createdDOCFolder = AppSettings("createdDOCFolder")
        logFolder = AppSettings("logFolder")

        If AppSettings("downloadXML") = "true" Then downloadXML = True
        If AppSettings("convert2XML") = "true" Then convert2XML = True
        If AppSettings("createDocs") = "true" Then createDocs = True
        If AppSettings("sendDocs") = "true" Then sendDocs = True

        'XML converter
        convertTipsDoc2XML = New TipsDoc2XML()
        convertTipsDoc2XML.outputFolder = tippebladXMLFolder

        'Doc creator
        docCreator = New CreateTipsDocuments()
        docCreator.outputFolder = createdDOCFolder

        'Modulewide arraylists
        xmlToFetch = New ArrayList()
        docsToSend = New ArrayList()

        'Load XML-addresses to fetch
        Dim n
        For n = 1 To AppSettings("count")
            xmlToFetch.Add(AppSettings("item" & n))
        Next

    End Sub

    'Sends all created documents by email to the specified recipient
    Sub SendDocsByMail()

        'Send docs?
        If docsToSend.Count > 0 Then

            Dim epost As MailMessage = New MailMessage()

            SmtpMail.SmtpServer = AppSettings("smtp")

            epost.From = AppSettings("fromEmail")
            epost.To = AppSettings("toEmail")
            epost.Subject = "Tippetips: Vurderingsdokumenter fra NTB Pluss"
            epost.BodyFormat = Web.Mail.MailFormat.Text
            epost.Body = "F�lgende tipsdokumenter er vedlagt:" & vbCrLf & vbCrLf

            Dim s As String
            For Each s In docsToSend
                epost.Body += s & vbCrLf
                Dim att As MailAttachment = New MailAttachment(s)
                epost.Attachments.Add(att)
            Next

            Dim ok As Boolean = True
            Try
                SmtpMail.Send(epost)
            Catch e As Exception
                ok = False

                'Log the error
                systemLog.WriteLine(Now & " : EMAIL - Failed to send docs by email")

                errorLog = New StreamWriter(logFolder & "errorlog.txt", True, Encoding.GetEncoding("iso-8859-1"))
                errorLog.WriteLine(Now & " : " & e.Message & vbCrLf & e.StackTrace & vbCrLf & vbCrLf)
                errorLog.Close()
            End Try

            If ok Then systemLog.WriteLine(Now & " : EMAIL - Email with documents sent to '" & AppSettings("toEmail") & "'")
        Else
            systemLog.WriteLine(Now & " : EMAIL - Skipped, no new documents to send")
        End If

    End Sub

    'Downloads and saves items from Norsk Tipping. Vurderingsdokumenter created if specified.
    Sub DownloadItems()

        'HTTP and stream stuff
        Dim request As HttpWebRequest
        Dim response As HttpWebResponse
        Dim resReader As StreamReader
        Dim fileWriter As StreamWriter

        'XML content
        Dim xmlDoc As XmlDocument = New XmlDocument()
        Dim content As String

        'Other helper vars
        Dim item As String
        Dim filename As String
        Dim dt As Date
        Dim week, day As String

        'Loop through the download object from the settings file and fetch them
        For Each item In xmlToFetch
            systemLog.WriteLine(Now & " : DOWNLOAD - Processing '" & item & "'")

            'Set up request
            request = WebRequest.Create(item)

            Try
                response = request.GetResponse()
            Catch e As WebException
                systemLog.WriteLine(Now & " : DOWNLOAD ERROR - " & e.Message)
                GoTo Next_Item
            End Try

            resReader = New StreamReader(response.GetResponseStream(), Encoding.GetEncoding("iso-8859-1"))
            content = resReader.ReadToEnd()
            resReader.Close()

            'Check XML content
            Try
                xmlDoc.LoadXml(content)
            Catch e As XmlException
                systemLog.WriteLine(Now & " : DOWNLOAD ERROR - " & e.Message)
                content = ""
            End Try

            'Check/fetch nodes
            Try
                dt = xmlDoc.SelectSingleNode("/TIPPING/DRAW_DATE").InnerText
                week = xmlDoc.SelectSingleNode("/TIPPING/WEEKNBR").InnerText
            Catch e As Exception
                systemLog.WriteLine(Now & " : DOWNLOAD ERROR - " & e.Message)
                content = ""
            End Try

            'Check if we have any valid xml-data
            If content <> "" Then
                day = days(dt.DayOfWeek)
                If week.Length < 2 Then week = "0" & week

                filename = "week-" & week & "_" & day & "_" & dt.ToString("dd-MM-yyyy") & ".xml"

                fileWriter = New StreamWriter(tippingXMLFolder & filename, False, Encoding.GetEncoding("iso-8859-1"))
                fileWriter.Write(content)
                fileWriter.Close()

                systemLog.WriteLine(Now & " : DOWNLOAD - " & filename & " saved.")

                'Create vurderingsdokument
                If createDocs Then
                    Dim ok As Boolean = True
                    Dim targetFile As String
                    Try
                        targetFile = docCreator.CreateTipsVurderingsDocument(tippingXMLFolder & filename)
                    Catch e As Exception
                        ok = False
                        docCreator.Cleanup()

                        'Log the error
                        systemLog.WriteLine(Now & " : DOC CREATION - Failed to create 'Vurderingsdokument' from " & filename)

                        errorLog = New StreamWriter(logFolder & "errorlog.txt", True, Encoding.GetEncoding("iso-8859-1"))
                        errorLog.WriteLine(Now & " : " & e.Message & vbCrLf & e.StackTrace & vbCrLf & vbCrLf)
                        errorLog.Close()
                    End Try

                    If ok Then
                        Dim span As TimeSpan = Now.Subtract(File.GetCreationTime(targetFile))
                        If span.TotalSeconds <= 5 Then docsToSend.Add(targetFile)
                        systemLog.WriteLine(Now & " : DOC CREATION - 'Vurderingsdokument' " & targetFile.Substring(targetFile.LastIndexOf("\") + 1) & " created.")
                    End If

                End If

            End If
Next_Item:
        Next

    End Sub

    'Creates XML docs from the tippeblad-docs in this folder 
    Sub ConvertTippebladDocs()

        'Loop the folder, create xml from docs
        Dim f, outfile As String
        Dim files As String() = Directory.GetFiles(tippebladDOCFolder)

        For Each f In files
            outfile = tippebladXMLFolder & f.Substring(f.LastIndexOf("\") + 1) & ".xml"
            systemLog.WriteLine(Now & " : XML CONVERSION - " & f.Substring(f.LastIndexOf("\") + 1) & " -> " & f.Substring(f.LastIndexOf("\") + 1) & ".xml")

            Dim ok As Boolean = True
            Try
                convertTipsDoc2XML.ConvertDoc2XML(f, outfile)
            Catch e As Exception
                ok = False
                convertTipsDoc2XML.Cleanup()

                'Log the error
                systemLog.WriteLine(Now & " : XML CONVERSION - Failed to create XML '" & f.Substring(f.LastIndexOf("\") + 1) & ".xml'")

                errorLog = New StreamWriter(logFolder & "errorlog.txt", True, Encoding.GetEncoding("iso-8859-1"))
                errorLog.WriteLine(Now & " : " & e.Message & vbCrLf & e.StackTrace & vbCrLf & vbCrLf)
                errorLog.Close()
            End Try

            If ok Then
                If File.Exists(tippebladDOCFolder & "used\" & f.Substring(f.LastIndexOf("\") + 1)) Then
                    File.Delete(tippebladDOCFolder & "used\" & f.Substring(f.LastIndexOf("\") + 1))
                End If
                File.Move(f, tippebladDOCFolder & "used\" & f.Substring(f.LastIndexOf("\") + 1))

                systemLog.WriteLine(Now & " : XML CONVERSION - OK! " & f.Substring(f.LastIndexOf("\") + 1) & " moved to " & tippebladDOCFolder & "used\")
            End If
        Next

    End Sub

    'Create fakta-docs from based on the tippeblad XML
    Sub CreateFaktaDocs()

        'Loop the folder, create xml from docs
        Dim f, outfile As String
        Dim files As String() = Directory.GetFiles(tippebladXMLFolder)

        For Each f In files

            systemLog.WriteLine(Now & " : DOC CREATION - Processing '" & f.Substring(f.LastIndexOf("\") + 1) & "'")

            'Find matching TippingXML
            Dim week = f.Substring(f.IndexOf(".") - 2, 2)
            Dim day = f.Substring(f.LastIndexOf("\") + 1, f.IndexOf(".") - 2 - f.LastIndexOf("\") - 1)

            Dim d As Integer
            If f.IndexOf("lor") > -1 Then
                d = 6
            ElseIf f.IndexOf("son") > -1 Then
                d = 0
            ElseIf f.IndexOf("man") > -1 Then
                d = 1
            Else
                d = 3
            End If

            Dim tf As String
            Dim tFiles As String() = Directory.GetFiles(tippingXMLFolder, "*week-" & week & "*")
            For Each tf In tFiles
                If d <> 3 And tf.IndexOf(days(d)) > -1 Then
                    Exit For
                    'ElseIf d = 6 Then
                    'If tf.IndexOf(days(d - 1)) > -1 Then Exit For
                ElseIf d = 0 Then
                    If tf.IndexOf(days(d + 1)) > -1 Then Exit For
                ElseIf d = 3 And tf.IndexOf(days(0)) = -1 And tf.IndexOf(days(1)) = -1 And _
                  tf.IndexOf(days(6)) = -1 Then
                    Exit For
                End If
            Next

            systemLog.WriteLine(Now & " : DOC CREATION - Matching Norsk Tipping XML found, '" & tf & "'")

            'Create faktadokument
            Dim filename
            Dim ok As Boolean = True
            Try
                filename = docCreator.CreateTipsFaktaDocument(tf, f)
            Catch e As Exception
                ok = False
                docCreator.Cleanup()

                'Log error
                systemLog.WriteLine(Now & " : DOC CREATION - Failed to create 'Faktadokument' from " & f)
                errorLog = New StreamWriter(logFolder & "errorlog.txt", True, Encoding.GetEncoding("iso-8859-1"))
                errorLog.WriteLine(Now & " : " & e.Message & vbCrLf & e.StackTrace & vbCrLf & vbCrLf)
                errorLog.Close()
            End Try

            'Log completion
            If ok Then
                systemLog.WriteLine(Now & " : DOC CREATION - 'Faktadokument' created from " & f)
                docsToSend.Add(filename)

                'Move the file if everything is okay
                If File.Exists(tippebladXMLFolder & "used\" & f.Substring(f.LastIndexOf("\") + 1)) Then
                    File.Delete(tippebladXMLFolder & "used\" & f.Substring(f.LastIndexOf("\") + 1))
                End If
                File.Move(f, tippebladXMLFolder & "used\" & f.Substring(f.LastIndexOf("\") + 1))

                systemLog.WriteLine(Now & " : DOC CREATION - " & f.Substring(f.LastIndexOf("\") + 1) & " moved to " & tippebladXMLFolder & "used\")
            End If

        Next

    End Sub

End Module
