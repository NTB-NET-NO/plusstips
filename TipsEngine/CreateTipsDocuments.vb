Imports System.IO
Imports System.Xml
Imports System.Text
Imports Word

'Creates 'fakta-' and 'vurderingsdokumenter'
Public Class CreateTipsDocuments

    Protected days As String() = {"S�ndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "L�rdag"}

    Protected wdApp As ApplicationClass
    Protected wdDoc As DocumentClass

    Public outputFolder As String

    'Constructor
    Public Sub New()
        wdApp = New Application()
    End Sub

    'Destructor
    Protected Overrides Sub Finalize()
        wdApp.Quit()
        wdApp = Nothing
        MyBase.Finalize()
    End Sub

    'Cleans up misc stuff to prevent errors
    Public Sub Cleanup()
        Try
            wdDoc.Close(Word.WdSaveOptions.wdDoNotSaveChanges)
        Catch e As Exception
        End Try
    End Sub

    'Creates the Fakta document based on XML from Norsk tipping and the document from Tippebladet Tips
    Public Function CreateTipsFaktaDocument(ByVal tippingXML As String, ByVal tippebladXML As String) As String

        'Get data from Norsk tippings XML document
        Dim xml As Xml.XmlDocument = New XmlDocument()
        xml.Load(tippingXML)

        Dim node As XmlNode = xml.SelectSingleNode("/TIPPING/WEEKNBR")
        Dim week As String = node.InnerText

        node = xml.SelectSingleNode("/TIPPING/DRAW_DATE")
        Dim dato As DateTime = node.InnerText

        'Tippetips' data
        xml.Load(tippebladXML)
        Dim bodyNode As XmlNode = xml.SelectSingleNode("/nitf/body/body.content")
        Dim nodeList As XmlNodeList

        'New Document
        wdDoc = wdApp.Documents.Add()

        'Generate content
        wdDoc.Range.InsertAfter("Tippetips fra NTB Pluss - Spilleomgang " & week & ", " & days(dato.DayOfWeek) & " " & dato.ToString("D") & vbCrLf)

        Dim p1 As Word.Paragraph = wdDoc.Paragraphs.Item(1)
        p1.Range.Font.Bold = True
        p1.Range.Font.Size = 16
        p1.Range.Font.Name = "Times New Roman"
        p1.Format.SpaceAfter = 16

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Color = WdColor.wdColorRed
        p1.Range.Text = "[tpk]"
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Color = WdColor.wdColorRed
        p1.Range.Text = "[tp00]"
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Text = "Denne " & days(dato.DayOfWeek).ToLower & "ens kupong (spilleomgang " & week & "):"
        p1.Range.Font.Bold = True
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Color = WdColor.wdColorRed
        p1.Range.Font.Bold = False
        p1.Range.Text = "[tp07]"
        p1.Range.InsertParagraphAfter()

        'Table - hovedkupongen
        Dim tablenode As XmlNode = bodyNode.SelectSingleNode("table[@class = 'hovedkupong']")
        Dim row, col As XmlNode
        Dim r, c As Integer

        Dim t1 As Word.Table = wdDoc.Tables.Add(wdDoc.Bookmarks.Item("\endofdoc").Range, 12, 3)
        t1.Range.Font.Bold = False
        t1.Borders.Enable = True

        r = 1
        c = 1
        For Each row In tablenode.ChildNodes
            For Each col In row.ChildNodes
                If c = 1 Then : t1.Cell(r, c).Range.Text = col.InnerText.Replace(".", "")
                Else : t1.Cell(r, c).Range.Text = col.InnerText
                End If
                c += 1
            Next
            r += 1
            c = 1
        Next

        t1.Columns.Item(1).Width = wdApp.CentimetersToPoints(2)
        t1.Columns.Item(2).Width = wdApp.CentimetersToPoints(5)
        t1.Columns.Item(2).Width = wdApp.CentimetersToPoints(5)

        'Spacing
        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.InsertParagraphAfter()

        'League tables
        Dim headerNodes As XmlNodeList = bodyNode.SelectNodes("hl3[@class = 'tabell']")
        Dim n As Integer = 0

        nodeList = bodyNode.SelectNodes("table[@class = 'tabell']")

        For Each node In nodeList

            p1 = wdDoc.Content.Paragraphs.Add()
            p1.Range.Font.Color = WdColor.wdColorRed
            p1.Range.Font.Bold = False
            p1.Range.Text = "[t201]"
            p1.Range.InsertParagraphBefore()
            p1.Range.InsertParagraphAfter()

            p1 = wdDoc.Content.Paragraphs.Add()
            p1.Range.Font.Color = WdColor.wdColorRed
            p1.Range.Font.Bold = False
            p1.Range.Text = "[t2u1]"
            p1.Range.InsertParagraphAfter()

            p1 = wdDoc.Content.Paragraphs.Add()
            p1.Range.Text = headerNodes.Item(n).InnerText
            p1.Range.Font.Bold = False
            p1.Range.InsertParagraphAfter()

            p1 = wdDoc.Content.Paragraphs.Add()
            p1.Range.Font.Color = WdColor.wdColorRed
            p1.Range.Font.Bold = False
            p1.Range.Text = "[t2u6]"
            p1.Range.InsertParagraphAfter()

            t1 = wdDoc.Tables.Add(wdDoc.Bookmarks.Item("\endofdoc").Range, node.ChildNodes.Count - 1, 20)
            t1.AllowAutoFit = True
            t1.AutoFitBehavior(WdAutoFitBehavior.wdAutoFitContent)
            t1.Borders.Enable = True
            t1.Range.Font.Bold = False

            r = 1
            c = 1
            For Each row In node.ChildNodes
                If r > 1 Then
                    For Each col In row.ChildNodes
                        If c > 1 Then t1.Cell(r - 1, c - 1).Range.Text = col.InnerText
                        c += 1
                    Next
                End If

                r += 1
                c = 1
            Next

            n += 1
        Next

        'Spacing
        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Text = vbCrLf & vbCrLf

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Color = WdColor.wdColorRed
        p1.Range.Font.Bold = False
        p1.Range.Text = "[tp00]"
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Text = bodyNode.SelectSingleNode("hl2[@class = 'femsiste']").InnerText
        p1.Range.Font.Bold = True
        p1.Range.InsertParagraphAfter()

        'Tables - fem siste
        nodeList = bodyNode.SelectNodes("table[@class = 'femsiste']")

        For Each node In nodeList

            Dim tmpVal As String

            p1 = wdDoc.Content.Paragraphs.Add()
            p1.Range.Font.Color = WdColor.wdColorRed
            p1.Range.Font.Bold = False
            p1.Range.Text = "[tp01]"
            p1.Range.InsertParagraphAfter()

            t1 = wdDoc.Tables.Add(wdDoc.Bookmarks.Item("\endofdoc").Range, node.ChildNodes.Count, 5)
            t1.AllowAutoFit = True
            t1.AutoFitBehavior(WdAutoFitBehavior.wdAutoFitContent)
            t1.Range.Font.Bold = False
            t1.Borders.Enable = True

            r = 1
            For Each row In node.ChildNodes

                If r = 1 Then
                    t1.Cell(r, 1).Range.Text = row.ChildNodes(0).InnerText
                Else
                    t1.Cell(r, 5).Range.Text = row.ChildNodes(0).InnerText

                    If row.ChildNodes.Count >= 3 Then
                        t1.Cell(r, 4).Range.Text = row.ChildNodes(2).InnerText

                        tmpVal = row.ChildNodes(1).InnerText
                        If tmpval.IndexOf("-") > -1 Then
                            t1.Cell(r, 2).Range.Text = tmpVal.Substring(0, tmpVal.IndexOf("-"))
                            t1.Cell(r, 3).Range.Text = tmpVal.Substring(tmpVal.IndexOf("-") + 1)
                        End If
                    End If

                End If

                r += 1
            Next

        Next

        'Spacing
        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Text = vbCrLf & vbCrLf

        'Formtabeller
        nodeList = bodyNode.SelectNodes("table[@class = 'formtabell']")

        'Header
        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Color = WdColor.wdColorRed
        p1.Range.Font.Bold = False
        p1.Range.Text = "[tp00]"
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Text = bodyNode.SelectSingleNode("hl2[@class = 'formtabell']").InnerText
        p1.Range.Font.Bold = True
        p1.Range.InsertParagraphAfter()

        For Each node In nodeList

            Dim tmpVal As String

            p1 = wdDoc.Content.Paragraphs.Add()
            p1.Range.Font.Color = WdColor.wdColorRed
            p1.Range.Font.Bold = False
            p1.Range.Text = "[tp02]"
            p1.Range.InsertParagraphAfter()

            t1 = wdDoc.Tables.Add(wdDoc.Bookmarks.Item("\endofdoc").Range, node.ChildNodes.Count, 7)
            t1.Range.Font.Bold = False
            t1.Borders.Enable = True

            r = 1
            For Each row In node.ChildNodes

                If row.ChildNodes.Count > 1 Then
                    t1.Cell(r, 1).Range.Text = row.ChildNodes(1).InnerText
                    t1.Cell(r, 2).Range.Text = row.ChildNodes(2).InnerText
                    t1.Cell(r, 3).Range.Text = row.ChildNodes(3).InnerText
                    t1.Cell(r, 4).Range.Text = row.ChildNodes(4).InnerText

                    tmpVal = row.ChildNodes(5).InnerText
                    t1.Cell(r, 5).Range.Text = tmpVal.Substring(0, tmpVal.IndexOf("-"))
                    t1.Cell(r, 6).Range.Text = tmpVal.Substring(tmpVal.IndexOf("-") + 1)

                    t1.Cell(r, 7).Range.Text = row.ChildNodes(6).InnerText
                Else
                    t1.Cell(r, 1).Range.Text = row.ChildNodes(0).InnerText
                End If

                r += 1
            Next

            t1.Columns.Item(1).Width = wdApp.CentimetersToPoints(4)
            t1.Columns.Item(2).Width = wdApp.CentimetersToPoints(1)
            t1.Columns.Item(3).Width = wdApp.CentimetersToPoints(1)
            t1.Columns.Item(4).Width = wdApp.CentimetersToPoints(1)
            t1.Columns.Item(5).Width = wdApp.CentimetersToPoints(1)
            t1.Columns.Item(6).Width = wdApp.CentimetersToPoints(1)
            t1.Columns.Item(7).Width = wdApp.CentimetersToPoints(1)

        Next

        'Spacing
        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Text = vbCrLf & vbCrLf

        'Tilsvarende kamper siste fem sesonger
        nodeList = bodyNode.SelectNodes("table[@class = 'tilsvarende']")

        'Header
        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Color = WdColor.wdColorRed
        p1.Range.Font.Bold = False
        p1.Range.Text = "[tp00]"
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Text = bodyNode.SelectSingleNode("hl2[@class = 'tilsvarende']").InnerText
        p1.Range.Font.Bold = True
        p1.Range.InsertParagraphAfter()


        For Each node In nodeList

            Dim tmpVal As String

            p1 = wdDoc.Content.Paragraphs.Add()
            p1.Range.Font.Color = WdColor.wdColorRed
            p1.Range.Font.Bold = False
            p1.Range.Text = "[tp03]"
            p1.Range.InsertParagraphAfter()

            t1 = wdDoc.Tables.Add(wdDoc.Bookmarks.Item("\endofdoc").Range, node.ChildNodes.Count, 12)
            t1.AllowAutoFit = True
            t1.AutoFitBehavior(WdAutoFitBehavior.wdAutoFitContent)
            t1.Range.Font.Bold = False
            t1.Borders.Enable = True

            r = 1
            For Each row In node.ChildNodes

                If r = 1 Then

                    If row.ChildNodes(2).InnerText.IndexOf("-") > -1 Then
                        t1.Cell(r, 3).Range.Text = row.ChildNodes(2).InnerText.Substring(2).Replace("-", "/")
                        t1.Cell(r, 5).Range.Text = row.ChildNodes(3).InnerText.Substring(2).Replace("-", "/")
                        t1.Cell(r, 7).Range.Text = row.ChildNodes(4).InnerText.Substring(2).Replace("-", "/")
                        t1.Cell(r, 9).Range.Text = row.ChildNodes(5).InnerText.Substring(2).Replace("-", "/")
                        t1.Cell(r, 11).Range.Text = row.ChildNodes(6).InnerText.Substring(2).Replace("-", "/")
                    Else
                        t1.Cell(r, 3).Range.Text = row.ChildNodes(2).InnerText
                        t1.Cell(r, 5).Range.Text = row.ChildNodes(3).InnerText
                        t1.Cell(r, 7).Range.Text = row.ChildNodes(4).InnerText
                        t1.Cell(r, 9).Range.Text = row.ChildNodes(5).InnerText
                        t1.Cell(r, 11).Range.Text = row.ChildNodes(6).InnerText
                    End If

                    t1.Cell(r, 3).Merge(t1.Cell(r, 4))
                    t1.Cell(r, 4).Merge(t1.Cell(r, 5))
                    t1.Cell(r, 5).Merge(t1.Cell(r, 6))
                    t1.Cell(r, 6).Merge(t1.Cell(r, 7))
                    t1.Cell(r, 7).Merge(t1.Cell(r, 8))

                Else
                    tmpVal = row.ChildNodes(1).InnerText
                    t1.Cell(r, 1).Range.Text = tmpVal.Substring(0, tmpVal.IndexOf("-")).Trim
                    t1.Cell(r, 2).Range.Text = tmpVal.Substring(tmpVal.IndexOf("-") + 1).Trim

                    If (row.ChildNodes.Count > 2) Then
                        tmpVal = row.ChildNodes(2).InnerText
                        If tmpVal.Length > 1 Then
                            t1.Cell(r, 3).Range.Text = tmpVal.Substring(0, tmpVal.IndexOf("-"))
                            t1.Cell(r, 4).Range.Text = tmpVal.Substring(tmpVal.IndexOf("-") + 1)
                        Else
                            t1.Cell(r, 3).Range.Text = "-"
                            t1.Cell(r, 4).Range.Text = "-"
                        End If

                        tmpVal = row.ChildNodes(3).InnerText
                        If tmpVal.Length > 1 Then
                            t1.Cell(r, 5).Range.Text = tmpVal.Substring(0, tmpVal.IndexOf("-"))
                            t1.Cell(r, 6).Range.Text = tmpVal.Substring(tmpVal.IndexOf("-") + 1)
                        Else
                            t1.Cell(r, 5).Range.Text = "-"
                            t1.Cell(r, 6).Range.Text = "-"
                        End If

                        tmpVal = row.ChildNodes(4).InnerText
                        If tmpVal.Length > 1 Then
                            t1.Cell(r, 7).Range.Text = tmpVal.Substring(0, tmpVal.IndexOf("-"))
                            t1.Cell(r, 8).Range.Text = tmpVal.Substring(tmpVal.IndexOf("-") + 1)
                        Else
                            t1.Cell(r, 7).Range.Text = "-"
                            t1.Cell(r, 8).Range.Text = "-"
                        End If

                        tmpVal = row.ChildNodes(5).InnerText
                        If tmpVal.Length > 1 Then
                            t1.Cell(r, 9).Range.Text = tmpVal.Substring(0, tmpVal.IndexOf("-"))
                            t1.Cell(r, 10).Range.Text = tmpVal.Substring(tmpVal.IndexOf("-") + 1)
                        Else
                            t1.Cell(r, 9).Range.Text = "-"
                            t1.Cell(r, 10).Range.Text = "-"
                        End If

                        tmpVal = row.ChildNodes(6).InnerText
                        If tmpVal.Length > 1 Then
                            t1.Cell(r, 11).Range.Text = tmpVal.Substring(0, tmpVal.IndexOf("-"))
                            t1.Cell(r, 12).Range.Text = tmpVal.Substring(tmpVal.IndexOf("-") + 1)
                        Else
                            t1.Cell(r, 11).Range.Text = "-"
                            t1.Cell(r, 12).Range.Text = "-"
                        End If
                    End If

                End If

                r += 1
            Next

        Next

        'Spacing
        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Text = vbCrLf & vbCrLf

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Color = WdColor.wdColorRed
        p1.Range.Font.Bold = False
        p1.Range.Text = "[tp00]"
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Text = bodyNode.SelectSingleNode("hl2[@class = 'toppscorere']").InnerText
        p1.Range.Font.Bold = True
        p1.Range.InsertParagraphAfter()

        'List - toppscorere
        nodeList = bodyNode.SelectNodes("table[@class = 'toppscorere']")

        For Each node In nodeList

            Dim tabs As Word.TabStops
            n = 1
            For Each row In node.ChildNodes
                Dim s As String = ""

                p1 = wdDoc.Content.Paragraphs.Add()
                If n = 1 Then
                    p1.Range.Font.Bold = True
                    'p1.Range.InsertParagraphBefore()
                Else
                    p1.Range.Font.Bold = False
                End If

                For Each col In row.ChildNodes
                    If s <> "" Then s += vbTab
                    s += col.InnerText
                Next

                p1.Range.Text = s
                p1.TabStops.Item(1).Position = wdApp.CentimetersToPoints(8)
                p1.TabStops.Item(2).Position = wdApp.CentimetersToPoints(9)
                p1.Range.InsertParagraphAfter()
                n += 1
            Next

        Next

        'Spacing
        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Text = vbCrLf & vbCrLf

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Color = WdColor.wdColorRed
        p1.Range.Font.Bold = False
        p1.Range.Text = "[tp00]"
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Text = bodyNode.SelectSingleNode("hl2[@class = 'neste_hovedkupong']").InnerText
        p1.Range.Font.Bold = True
        p1.Range.InsertParagraphAfter()

        'Table - next week
        nodeList = bodyNode.SelectNodes("table[@class = 'neste_hovedkupong']")

        For Each node In nodeList

            If node.ChildNodes.Count = 1 Then
                p1 = wdDoc.Content.Paragraphs.Add()
                p1.Range.Font.Color = WdColor.wdColorRed
                p1.Range.Font.Bold = False
                p1.Range.Text = "[tp00]"
                p1.Range.InsertParagraphAfter()

                p1 = wdDoc.Content.Paragraphs.Add()
                p1.Range.Font.Bold = False
                p1.Range.Text = node.FirstChild.FirstChild.InnerText
                p1.Range.InsertParagraphAfter()

            Else
                p1 = wdDoc.Content.Paragraphs.Add()
                p1.Range.Font.Color = WdColor.wdColorRed
                p1.Range.Font.Bold = False
                p1.Range.Text = "[tp07]"
                p1.Range.InsertParagraphAfter()

                t1 = wdDoc.Tables.Add(wdDoc.Bookmarks.Item("\endofdoc").Range, 12, 3)
                t1.Range.Font.Bold = False
                t1.Borders.Enable = True

                r = 1
                c = 1
                For Each row In node.ChildNodes
                    For Each col In row.ChildNodes
                        If c = 1 Then : t1.Cell(r, c).Range.Text = col.InnerText.Replace(".", "")
                        Else : t1.Cell(r, c).Range.Text = col.InnerText
                        End If
                        c += 1
                    Next
                    r += 1
                    c = 1
                Next

                t1.Columns.Item(1).Width = wdApp.CentimetersToPoints(2)
                t1.Columns.Item(2).Width = wdApp.CentimetersToPoints(5)
                t1.Columns.Item(2).Width = wdApp.CentimetersToPoints(5)

            End If

        Next

        'Done - Save the doc
        Dim filename
        If week.Length < 2 Then week = "0" & week
        filename = outputFolder & "fakta-omgang_" & week & "-" & dato.ToString("dd-MM-yyyy") & ".doc"

        wdDoc.SaveAs(filename)
        wdDoc.Close()

        Return filename
    End Function


    'Creates the Vurderingsdokument based on XML from Norsk tipping
    Public Function CreateTipsVurderingsDocument(ByVal tippingXML As String) As String

        'Get data from Norsk tippings XML document
        Dim xml As Xml.XmlDocument = New XmlDocument()
        xml.Load(tippingXML)

        Dim node As XmlNode = xml.SelectSingleNode("/TIPPING/WEEKNBR")
        Dim week As String = node.InnerText

        node = xml.SelectSingleNode("/TIPPING/DRAW_DATE")
        Dim dato As DateTime = node.InnerText

        'New Document
        wdDoc = wdApp.Documents.Add()

        'Generate content
        wdDoc.Range.InsertAfter("Tippetips fra NTB Pluss - Spilleomgang " & week & ", " & days(dato.DayOfWeek) & " " & dato.ToString("D") & vbCrLf)

        Dim p1 As Word.Paragraph = wdDoc.Paragraphs.Item(1)
        p1.Range.Font.Bold = True
        p1.Range.Font.Size = 16
        p1.Range.Font.Name = "Times New Roman"
        p1.Format.SpaceAfter = 16

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Color = WdColor.wdColorRed
        p1.Range.Text = "[tpk]"
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Color = WdColor.wdColorRed
        p1.Range.Text = "[tp00]"
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Text = "Denne " & days(dato.DayOfWeek).ToLower & "ens kupong (spilleomgang " & week & "):"
        p1.Range.Font.Bold = False
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Text = "Vurdert av (navn), NTB Pluss"
        p1.Range.Font.Bold = True
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Color = WdColor.wdColorRed
        p1.Range.Font.Bold = False
        p1.Range.Text = "[vurd]"
        p1.Range.InsertParagraphAfter()

        'Table - vurdering av hovedkupongen
        Dim nodeList As XmlNodeList = xml.SelectNodes("/TIPPING/TIP_EVENT")
        Dim row As XmlNode
        Dim r, c As Integer

        Dim t1 As Word.Table = wdDoc.Tables.Add(wdDoc.Bookmarks.Item("\endofdoc").Range, 3 * 12, 5)
        t1.AllowAutoFit = True
        t1.AutoFitBehavior(WdAutoFitBehavior.wdAutoFitContent)
        t1.Range.Font.Bold = False
        t1.Borders.Enable = True

        r = 1
        c = 1
        For Each node In nodeList

            'Data about the matches
            row = node.SelectSingleNode("./TIP_EVENT_NBR")

            t1.Cell(r, c).Range.Text = row.InnerText.Trim()
            c += 1

            Dim tmp As String
            row = node.SelectSingleNode("./MATCH/HOME_TEAM/TEAM_NAME")
            tmp = row.InnerText.Trim()

            row = node.SelectSingleNode("./MATCH/AWAY_TEAM/TEAM_NAME")
            tmp += " - " & row.InnerText.Trim()

            t1.Cell(r, c).Range.Font.Bold = True
            t1.Cell(r, c).Range.Text = tmp
            c += 1

            'Other stuff in the table
            t1.Cell(r, c).Range.Text = "Utgangstips"
            t1.Cell(r, c).Range.Font.Italic = True
            c += 1

            t1.Cell(r, c).Range.Text = "Gardering"
            t1.Cell(r, c).Range.Font.Italic = True
            c += 1

            t1.Cell(r, c).Range.Text = "Odds"
            t1.Cell(r, c).Range.Font.Italic = True

            c = 1
            r += 1
            t1.Cell(r, c).Range.Text = "(Vurdering)"

            'Merge some cells
            Dim i As Integer
            For i = 1 To 4
                t1.Cell(r, c).Merge(t1.Cell(r, c + 1))
            Next

            r += 1
            For i = 1 To 4
                t1.Cell(r, c).Merge(t1.Cell(r, c + 1))
            Next

            r += 1
        Next

        'Spacing
        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Text = vbCrLf & vbCrLf

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Color = WdColor.wdColorRed
        p1.Range.Font.Bold = False
        p1.Range.Text = "[forsl]"
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Bold = True
        p1.Range.Text = "Ukens kupongforslag:"
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Bold = True
        p1.Range.Font.Italic = True
        p1.Range.Text = "# Mrk: Utgangsrekke legges automatisk inn fra tabellen over #"
        p1.Range.InsertParagraphAfter()

        'Spacing
        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Text = vbCrLf & vbCrLf

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Bold = True
        p1.Range.Text = "72 rekker:"
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Color = WdColor.wdColorRed
        p1.Range.Font.Bold = False
        p1.Range.Text = "[tp10]"
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Bold = True
        p1.Range.Font.Italic = True
        p1.Range.Text = "# Mrk: Sett kun inn garderinger (ikke utgangsrekken) #"
        p1.Range.InsertParagraphAfter()

        t1 = wdDoc.Tables.Add(wdDoc.Bookmarks.Item("\endofdoc").Range, 12, 2)
        t1.Range.Font.Bold = False
        t1.Borders.Enable = True
        t1.Columns.Item(1).Width = wdApp.CentimetersToPoints(1)
        t1.Columns.Item(2).Width = wdApp.CentimetersToPoints(2)

        Dim n = 0
        For n = 1 To 12
            t1.Cell(n, 1).Range.Text = n & "."
        Next

        'Spacing
        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Text = vbCrLf & vbCrLf

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Bold = True
        p1.Range.Text = "432 rekker:"
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Color = WdColor.wdColorRed
        p1.Range.Font.Bold = False
        p1.Range.Text = "[tp10]"
        p1.Range.InsertParagraphAfter()

        p1 = wdDoc.Content.Paragraphs.Add()
        p1.Range.Font.Bold = True
        p1.Range.Font.Italic = True
        p1.Range.Text = "# Mrk: Sett kun inn garderinger (ikke utgangsrekken) #"
        p1.Range.InsertParagraphAfter()

        t1 = wdDoc.Tables.Add(wdDoc.Bookmarks.Item("\endofdoc").Range, 12, 2)
        t1.Range.Font.Bold = False
        t1.Borders.Enable = True
        t1.Columns.Item(1).Width = wdApp.CentimetersToPoints(1)
        t1.Columns.Item(2).Width = wdApp.CentimetersToPoints(2)

        For n = 1 To 12
            t1.Cell(n, 1).Range.Text = n & "."
        Next

        'Done - Save the doc
        Dim filename
        If week.Length < 2 Then week = "0" & week
        filename = outputFolder & "vurdering-omgang_" & week & "-" & dato.ToString("dd-MM-yyyy") & ".doc"

        wdDoc.SaveAs(filename)
        wdDoc.Close()

        Return filename
    End Function

End Class
